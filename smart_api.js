// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
var auth = require('basic-auth')			//basic authentication
var url = require('url');

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
//app.use(bodyParser.json());

var port = process.env.PORT || 8080;        // set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

router.get('/', function(req, res) {
	var credentials = auth(req)

	if (!credentials || credentials.name !== 'john' || credentials.pass !== 'nonlaso') {
	    res.statusCode = 401
	    res.setHeader('WWW-Authenticate', 'Basic realm="example"')
	    res.end('Access denied')
  	} else {
  		var queryData = url.parse(req.url, true).query;

  		if (queryData.query) {
			res.type('text/plain');
	    	res.send('Preset Positions for camera 1\npresetposno1=Home\npresetposno2=Posizione2 \npresetposno3=Posizione3\npresetposno4=Posizione4');
  		};

  		if (queryData.gotoserverpresetname) {
  			res.type('text/plain');
	    	res.send('Preset received correctly');
  		};
	  	
	  	res.end('Access granted')
  	}
});

// more routes for our API will happen here

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/axis-cgi/com/ptz.cgi', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);
 
