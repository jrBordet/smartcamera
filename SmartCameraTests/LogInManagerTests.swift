//
//  LogInManagerTests.swift
//  SmartCamera
//
//  Created by Jean Raphael Bordet on 08/09/15.
//  Copyright (c) 2015 Jean Raphael Bordet. All rights reserved.
//

import XCTest

class LogInManagerTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    func logInManagerExample() {
        // This is an example of a functional test case.
        XCTAssert(true, "Pass")
        
        // given
        LogInManager.sharedInstance.completeWithUsername("jr", password: "pass", hostname: "host", port: "port")
        LogInManager.sharedInstance.port = "80"
        
        // then
        XCTAssertEqual(LogInManager.sharedInstance.username, "jr", "Verify username read and write")
        XCTAssertEqual(LogInManager.sharedInstance.password, "pass", "Verify password read and write")
        XCTAssertEqual(LogInManager.sharedInstance.hostname, "host", "Verify password read and write")
        XCTAssertEqual(LogInManager.sharedInstance.port, "port", "Verify password read and write")

        XCTAssertEqual(LogInManager.sharedInstance.port, "80", "Verify PORT default initalization")
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

}
