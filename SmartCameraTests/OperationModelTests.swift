//
//  OperationModelTests.swift
//  SmartCamera
//
//  Created by Jean Raphael Bordet on 08/09/15.
//  Copyright (c) 2015 Jean Raphael Bordet. All rights reserved.
//

import UIKit
import XCTest

class OperationModelTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        for i in 0...9 {
            OperationStore.sharedInstance.add(Operation(name: "Home \(i)",number: i))
        }
    }

    func testInitOperationModel() {
        // given
        let operation = Operation(name: "Home", number: 1)
        
        // then
        XCTAssertEqual(operation.name, "Home", "Check Operation.name")
        XCTAssertEqual(operation.number, 1, "Check Operation.number")
    }
    
    func testNumberOfElementsModelStore() {
        // then
        XCTAssertEqual(OperationStore.sharedInstance.numberOfElements(), 10, "Number of Elements in store failed")
    }
    
    func testElementAtIndexModelStore() {
        // then
        guard let _ = OperationStore.sharedInstance.getElementAt(0) else {
            XCTAssertTrue(false)
            return
        }
        
        XCTAssertEqual(OperationStore.sharedInstance.getElementAt(0)!.name, "Home 0", "Number at Index in store failed")
    }
    
    func testElementName() {
        // given
        let indexPath = NSIndexPath(forRow: 0, inSection: 0)
        let indexPath1 = NSIndexPath(forRow: 9, inSection: 0)
        
        let name1 = OperationStore.sharedInstance.getElementAtIndexPath(indexPath)
        let name2 = OperationStore.sharedInstance.getElementAtIndexPath(indexPath1)
        
        // when
        XCTAssertEqual(name1, "Home 0", "")
        XCTAssertEqual(name2, "Home 9", "")
    }
    
    override func tearDown() {
        super.tearDown()
        
        OperationStore.sharedInstance.items = []
    }
}
