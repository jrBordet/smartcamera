//
//  OperationStoreTest.swift
//  SmartCamera
//
//  Created by Jean Raphael Bordet on 16/09/15.
//  Copyright (c) 2015 Jean Raphael Bordet. All rights reserved.
//

import XCTest
import XCTest

class OperationStoreTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    /*func testgetElements() {
        OperationStore.sharedInstance.delegate = self
        
        OperationStore.sharedInstance.getElements("http://localhost", port: "80")
        
        waitForExpectationsWithTimeout(10.0, handler:nil)
        XCTAssert(true, "Pass")
    }*/
    
    //MARK: OperationStoreDelegate methods
    /*func downloadCompleted(elements: [Operation]?) {
        let expectation = expectationWithDescription("Swift Expectations")

        expectation.fulfill()

        XCTAssertEqual(OperationStore.sharedInstance.operationTitle, "Preset Positions for camera 1", "Title error during parsing operations")
        
        XCTAssertEqual(OperationStore.sharedInstance.items[0].name, "Home", "Error during parsing operations")
        
        if let elements = elements {
            XCTAssertEqual(elements[0].name, "Home", "Error during parsing operations")
        } else {
        
            XCTAssertTrue(false, "Fail download elements")
        }
        
        print("downloadCompleted")
    }*/
    
    func testParseOperation() {
        var result: [Operation] = []

        result = OperationStore.sharedInstance.parseOperation("Preset Positions for camera 1\n" +
            "presetposno1=   Home   \n" +
            "  presetposno2= Posizione2")
            
        XCTAssertEqual(result[0].name, "Home", "Error during parsing operations")
        XCTAssertEqual(result[1].name, "Posizione2", "Error during parsing operations")
    }
}
