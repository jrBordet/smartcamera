//
//  HttpClientTests.swift
//  SmartCamera
//
//  Created by Jean Raphael Bordet on 10/09/15.
//  Copyright (c) 2015 Jean Raphael Bordet. All rights reserved.
//

import XCTest

class HttpClientTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    func testRequestPresets() {
        HttpClient.Instance.composeEnpoint(Url: "localhost", port: "8080", user: "john", pass: "nonlaso")

        let expectation = expectationWithDescription("Swift Expectations")
        
        HttpClient.Instance.requestPresets({ (response: AnyObject!, code: Int?) in
            
            if let resp: AnyObject = response {
                let newStr = NSString(data: resp as! NSData, encoding: NSUTF8StringEncoding)
                print(newStr, terminator: "")
            }
            
            XCTAssertNotNil(response, "Fail download presets")
            
            expectation.fulfill()
        })
        waitForExpectationsWithTimeout(10.0, handler:nil)
    }
    
    func testErrorRequestPresets() {
        HttpClient.Instance.composeEnpoint(Url: "localhost", port: "8080", user: "john", pass: "wrong")

        let expectation = expectationWithDescription("Swift Expectations")
        
        HttpClient.Instance.requestPresets({ (response: AnyObject!, code: Int?) in
            
            if let resp: AnyObject = response {
                    let newStr = NSString(data: resp as! NSData, encoding: NSUTF8StringEncoding)
                    print(newStr, terminator: "")
            }
            
            XCTAssertNil(response, "Response should be nil")
            
            expectation.fulfill()
        })
        waitForExpectationsWithTimeout(10.0, handler:nil)
    }
    
    func testGetRequest() {
        HttpClient.Instance.composeEnpoint(Url: "localhost", port: "8080", user: "john", pass: "nonlaso")
        let expectation = expectationWithDescription("Swift Expectations")

        let response = { (response: AnyObject!, code: Int?) ->() in
        
            if let resp: AnyObject = response {
                let newStr = NSString(data: resp as! NSData, encoding: NSUTF8StringEncoding)
                print(newStr, terminator: "")
            }
            
            expectation.fulfill()
        }
        
        HttpClient.Instance.getRequest(Query: "?query=presetposall", completion: response)
        
        waitForExpectationsWithTimeout(10.0, handler:nil)
    }
    
    func testSendPresets() {
        HttpClient.Instance.composeEnpoint(Url: "localhost", port: "8080", user: "john", pass: "nonlaso")
        
        let expectation = expectationWithDescription("Swift Expectations")
        
        HttpClient.Instance.sendPresets("Home", completion: { (response: AnyObject!, code: Int?) in
            
            if let resp: AnyObject = response {
                let newStr = NSString(data: resp as! NSData, encoding: NSUTF8StringEncoding) as! String
                XCTAssertEqual(newStr, "Preset received correctly", "")
            }
            
            XCTAssertNotNil(response, "Fail send presets")
            
            expectation.fulfill()
        })
        waitForExpectationsWithTimeout(10.0, handler:nil)
    }
    
    func testComposeEnpoint() {
        HttpClient.Instance.composeEnpoint(Url: "localhost", port: "8080", user: "john", pass: "nonlaso")
        
        if let endpoint = HttpClient.Instance.endpoint {
            XCTAssertEqual(endpoint, "http://john:nonlaso@localhost:8080/axis-cgi/com/ptz.cgi", "Error dunring compose enpoint")
        }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
}
