//
//  ViewController.swift
//  SmartCamera
//
//  Created by Jean Raphael Bordet on 08/09/15.
//  Copyright (c) 2015 Jean Raphael Bordet. All rights reserved.
//

import UIKit

class LogInViewController: UIViewController, UITextFieldDelegate, StoreDownloadDelegate {

    //MARK: IBOutlets
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var hostnameTextField: UITextField!
    @IBOutlet weak var portTextField: UITextField!

    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    
    //MARK: UIViewController methodss
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.usernameTextField.delegate = self
        self.passwordTextField.delegate = self
        self.hostnameTextField.delegate = self
        self.portTextField.delegate = self

         // Init TextFields with User Defaults
        initTextField()
        
        // Function downloadCompleted will be called when all preset are available
        OperationStore.sharedInstance.downloadDelegate = self
        performLoading(State: true)
    }
    
    private func initTextField(){
        self.usernameTextField.text = LogInManager.sharedInstance.username
        self.passwordTextField.text = LogInManager.sharedInstance.password
        self.hostnameTextField.text = LogInManager.sharedInstance.hostname
        self.portTextField.text = LogInManager.sharedInstance.port
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: UITextFieldDelegate methods
    func textFieldShouldReturn(userText: UITextField) -> Bool {
        
        usernameTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        hostnameTextField.resignFirstResponder()
        portTextField.resignFirstResponder()
        
        return true;
    }
    
    //MARK: LogIn actions
    @IBAction func LogInAction(sender: UIButton) {
        downloadPreset()
    }
    
    func saveUserlogIn() {
        LogInManager.sharedInstance.completeWithUsername(usernameTextField.text!,
            password: passwordTextField.text!,
            hostname: hostnameTextField.text!,
            port: portTextField.text!)
        
        LogInManager.sharedInstance.description()
    }
    
    func downloadPreset() {
        resetElements()
        
        HttpClient.Instance.composeEnpoint(Url: hostnameTextField.text!, port: portTextField.text!, user: usernameTextField.text!, pass: passwordTextField.text!)

        OperationStore.sharedInstance.downloadElements()
        
        performLoading(State: false)
    }
    
    func resetElements() {
        OperationStore.sharedInstance.items = nil
    }
    
    //MARK: OperationStoreDelegate methods
    func downloadCompleted(elements: [Operation]?, code: Int) {
        performLoading(State: true)
        
        if let _ = elements {
            saveUserlogIn()

            performSegueWithIdentifier("presetSegue", sender: self)
        } else {
            let alert = UIAlertController(title: "Error", message: "some error occur during presets download", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: Loading indicator
    func performLoading(State state: Bool) {
        if state {
            loadingActivityIndicator.hidden = true
            loadingActivityIndicator.stopAnimating()
        } else {
            loadingActivityIndicator.hidden = false
            loadingActivityIndicator.startAnimating()
        }
    }
}

