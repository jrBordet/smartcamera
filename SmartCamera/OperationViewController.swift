//
//  OperationViewController.swift
//  SmartCamera
//
//  Created by Jean Raphael Bordet on 08/09/15.
//  Copyright (c) 2015 Jean Raphael Bordet. All rights reserved.
//

import UIKit

class OperationViewController: UITableViewController, StoreSendDelegate {

    //MARK: IBOutlets
    @IBOutlet var modelViewObject: OperationViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.modelViewObject = OperationViewModel()
        
        // Function sendPresetsCompleted will be called when all preset are available
        OperationStore.sharedInstance.sendDelegate = self
        
        self.title = modelViewObject.titleFotNavigationController()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UITableView DataSource
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modelViewObject.numberOfItemsInSection(section)
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("operationCell", forIndexPath: indexPath) as! OperationCell
        
        configureCell(cell, forRowATindexpath: indexPath)
        
        return cell
    }
    
    func configureCell(cell: OperationCell, forRowATindexpath indexPath: NSIndexPath){
        cell.titleLabel.text = modelViewObject.titleForItemAtIndexPath(indexPath)
    }
    
    //MARK: OperationStoreDelegate methods
    func sendPresetsCompleted(Status response: String, code: Int) {
        let cell = tableView.cellForRowAtIndexPath(self.modelViewObject.indexPath!) as! OperationCell
        
        if (code / 100) == 2 {
            cell.statusImage.backgroundColor = UIColor.greenColor()
            cell.statusImage.layer.borderColor = UIColor.greenColor().CGColor

        } else {
            cell.statusImage.backgroundColor = UIColor.redColor()
            cell.statusImage.layer.borderColor = UIColor.redColor().CGColor
        }
    }
    //MARK: UITableViewController methods
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.modelViewObject.indexPath = indexPath
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! OperationCell
        
        cell.statusImage.backgroundColor = UIColor.yellowColor()
        cell.statusImage.layer.borderColor = UIColor.yellowColor().CGColor
        
        let name = OperationStore.sharedInstance.getElementAt(indexPath.row)?.name
        
        OperationStore.sharedInstance.sendPresets(Name: name!)
    }
    
    override func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! OperationCell
        
        cell.statusImage.backgroundColor = UIColor.whiteColor()
        cell.statusImage.layer.borderColor = UIColor.grayColor().CGColor
    }
}
