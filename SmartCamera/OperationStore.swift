//
//  OperationStore.swift
//  SmartCamera
//
//  Created by Jean Raphael Bordet on 08/09/15.
//  Copyright (c) 2015 Jean Raphael Bordet. All rights reserved.
//

import Foundation

protocol StoreDownloadDelegate {
    func downloadCompleted(elements: [Operation]?, code: Int)
}

protocol StoreSendDelegate {
    func sendPresetsCompleted(Status response: String, code: Int)
}

class OperationStore {
    static let sharedInstance = OperationStore()
    
    var items: [Operation]? = []
    var operationTitle: String = "Default Preset"
    
    var downloadDelegate: StoreDownloadDelegate?
    var sendDelegate: StoreSendDelegate?
    
    init() {
    }
    
    func add(operation: Operation) {
        //self.items?.append(operation)
        
        if let _ = self.items {
            self.items?.append(operation)
        }
    }
    
    func downloadElements() {
        HttpClient.Instance.requestPresets({(response: AnyObject!, code: Int?) in

            if let _: AnyObject = response {
                self.items = self.parseOperation(NSString(data: response as! NSData, encoding: NSUTF8StringEncoding) as! String)
            }
            
            if let code = code {
                self.downloadDelegate?.downloadCompleted(self.items, code: code)
            } else {
                self.downloadDelegate?.downloadCompleted(self.items, code: 404)
            }
        })
    }
    
    func parseOperation(text: String) -> [Operation] {
        var result: [Operation] = []
        
        var resultBreakReturn = text.componentsSeparatedByString("\n")
        
        self.operationTitle = resultBreakReturn[0]
        
        for (index, value) in resultBreakReturn.enumerate() {
            if(index != 0) {
                var rowComponents: [String] = value.componentsSeparatedByString("=")
                if (rowComponents.count == 2) {
                    let trim = rowComponents[1].stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
                    result.append(Operation(name: trim, number: index))
                }
            } 
        }
        
        return result
    }
    
    func sendPresets(Name name: String) {
        HttpClient.Instance.sendPresets(name, completion: {(response: AnyObject!, code: Int?) in
            
            if let _: AnyObject = response {
                let response = NSString(data: response as! NSData, encoding: NSUTF8StringEncoding) as! String
                if let code = code {
                    self.sendDelegate?.sendPresetsCompleted(Status: response, code: code)
                } else {
                    self.sendDelegate?.sendPresetsCompleted(Status: response, code: 404)
                }
            }
        })
    }
    
    func getElementAt(Index: Int) -> Operation? {
         if let items = self.items {
            return items[Index]
         } else { return nil }
    }
    
    func getElementAtIndexPath(IndexPath: NSIndexPath) -> String {
         if let items = self.items {
            return items[IndexPath.row].name
         } else { return "" }
    }
    
    func numberOfElements() -> Int {
         if let items = self.items {
            return items.count
         }
        return 0
    }
}