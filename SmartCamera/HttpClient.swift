//
//  HttpClient.swift
//  SmartCamera
//
//  Created by Jean Raphael Bordet on 10/09/15.
//  Copyright (c) 2015 Jean Raphael Bordet. All rights reserved.
//

import Foundation

class HttpClient {
    let manager = AFHTTPRequestOperationManager()
    var endpoint: String?
    
    class var Instance: HttpClient {
        struct Singleton {
            static let instance = HttpClient()
        }
        return Singleton.instance
    }
    
    func requestPresets(completion: (response: AnyObject!, Int?) -> ()) {
        getRequest(Query: "?query=presetposall", completion: completion)
    }
    
    func sendPresets(name:String, completion: (response: AnyObject!, Int?) -> ()) {
        getRequest(Query: "?gotoserverpresetname=" + name, completion: completion)
    }
    
    func getRequest(Query query: String, completion: (response: AnyObject!, code: Int?) -> ()) {
        manager.responseSerializer = AFHTTPResponseSerializer()
        
        if let endpoint = self.endpoint {
            manager.GET(endpoint + query,
                parameters: nil,
                success: { (operation: AFHTTPRequestOperation!, responseObject: AnyObject!) in
                    completion(response: responseObject, code: operation.response?.statusCode)
                },
                failure: { (operation: AFHTTPRequestOperation!, error: NSError!) in
                    print("\nError: " + error.localizedDescription)
                    completion(response: nil, code: operation.response?.statusCode)
            })
        }
    }
    
    func composeEnpoint(Url url: String, port: String, user: String, pass: String ) {
        self.endpoint = "http://" + user + ":" + pass + "@" + url + ":" + port + "/axis-cgi/com/ptz.cgi"
    }
}