//
//  OperationViewModel.swift
//  SmartCamera
//
//  Created by Jean Raphael Bordet on 08/09/15.
//  Copyright (c) 2015 Jean Raphael Bordet. All rights reserved.
//

import UIKit

class OperationViewModel: NSObject {
    
    var indexPath: NSIndexPath?
    
    func numberOfItemsInSection(section: Int) -> Int {
        return OperationStore.sharedInstance.numberOfElements()
    }
    
    func titleForItemAtIndexPath(IndexPath: NSIndexPath) -> String {
        return OperationStore.sharedInstance.getElementAtIndexPath(IndexPath)
    }
    
    func titleFotNavigationController() -> String {
        return OperationStore.sharedInstance.operationTitle
    }
}
