//
//  Operation.swift
//  SmartCamera
//
//  Created by Jean Raphael Bordet on 08/09/15.
//  Copyright (c) 2015 Jean Raphael Bordet. All rights reserved.
//

import Foundation

class Operation {
    
    var name: String
    var number: Int
    
    init(name: String, number: Int){
        self.name = name
        self.number = number
    }
}
