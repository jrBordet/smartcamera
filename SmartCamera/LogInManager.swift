//
//  LogInManager.swift
//  SmartCamera
//
//  Created by Jean Raphael Bordet on 08/09/15.
//  Copyright (c) 2015 Jean Raphael Bordet. All rights reserved.
//

import Foundation

class LogInManager {
    
    private let USERNAME_KEY = "usernameKey"
    private let PASSWORD_KEY = "passwordKey"
    private let HOSTNAME_KEY = "hostnameKey"
    private let PORT_KEY = "portKey"

    static let sharedInstance = LogInManager()
    
    let defaults = NSUserDefaults.standardUserDefaults()
    
    func description() {
        print("username: \(username)\npassword: \(password)\nhostname: \(hostname)\nport: \(port)", terminator: "")
    }
    
    func completeWithUsername(u: String, password: String, hostname: String, port: String){
        defaults.setObject(u, forKey: USERNAME_KEY)
        defaults.setObject(password, forKey: PASSWORD_KEY)
        defaults.setObject(hostname, forKey: HOSTNAME_KEY)
        defaults.setObject(port, forKey: PORT_KEY)
    }
    
    var username: String{
        get{
             return defaults.stringForKey(USERNAME_KEY) ?? ""
        }
        set(newUsername){
            defaults.setObject(newUsername, forKey: USERNAME_KEY)
        }
    }
    
    var password: String{
        get{
            return defaults.stringForKey(PASSWORD_KEY) ?? ""
        }
        set(newPassword){
            defaults.setObject(newPassword, forKey: PASSWORD_KEY)
        }
    }
    
    var hostname: String{
        get{
             return defaults.stringForKey(HOSTNAME_KEY) ?? "localhost"
        }
        set(newHostName){
            defaults.setObject(newHostName, forKey: HOSTNAME_KEY)
        }
    }
    
    var port: String{
        get{
            return defaults.stringForKey(PORT_KEY) ?? "8080"
        }
        set(newPort){
            defaults.setObject(newPort, forKey: PORT_KEY)
        }
    }
}