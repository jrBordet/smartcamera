//
//  OperationCell.swift
//  SmartCamera
//
//  Created by Jean Raphael Bordet on 16/09/15.
//  Copyright (c) 2015 Jean Raphael Bordet. All rights reserved.
//

import UIKit

class OperationCell: UITableViewCell {
    var statusImage = UIImageView()
    var titleLabel = UILabel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        statusImage = UIImageView(frame: CGRectMake(0, 0, 10, 10))
        
        statusImage.layer.cornerRadius = CGRectGetWidth(statusImage.frame)/2
        statusImage.layer.masksToBounds = true
        statusImage.layer.borderWidth = 1
        
        statusImage.layer.borderColor = UIColor.grayColor().CGColor
        statusImage.backgroundColor = UIColor.whiteColor()

        statusImage.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.addSubview(statusImage)
        contentView.addSubview(titleLabel)
        
        let viewsDict = [
            "image" : statusImage,
            "username" : titleLabel
        ]
        
        contentView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-[image(10)]", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewsDict))
        contentView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-[username]-[image(10)]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewsDict))
    }
}
